# Project Athens


This repo contains project's that are effectively serve only me, such as my
personal blog, public CV, and some other project's that are largely
"self-serving". 

# Infrastructure High Level

Iterations of this infra have gone through Fargate Clusters, Kubernetes
Clusters, but these days I host on Vultr with Docker because I want to keep
things simple and cost effective. Sample clusters and previous versions 
of my infrastructure are preserved in this repository however as I do use
that code for other projects of mine :smiley:

## Nginx Reverse Proxy service

The following websites are served from this repository

* Personal /funposting website/ -  [link](https://temper.tv)
* Personal Devloper Blog - [link](https://shockrah.xyz)
* Developer Portfolio - [link](https://resume.shockrah.xyz)
* Gitea instance - [link](https://git.shockrah.xyz)


# Why Temper & Shockrah?

I keep developer related things under the Shockrah name and /funposting/ is kept
to the Temper name.

