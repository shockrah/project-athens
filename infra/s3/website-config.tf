##################################################################
# Below are the acl components for each bucket to make them public
##################################################################

# TODO: ensure proper dependency chaining to the buckets that these
# blocks require to be in place _before_ they come up

# Enables website configuration
resource "aws_s3_bucket_website_configuration" "site" {
  for_each = aws_s3_bucket.static-content
  bucket = each.value.bucket
  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "404.html"
  }
}

# Set block public access to false
resource "aws_s3_bucket_public_access_block" "site" {
  for_each = aws_s3_bucket.static-content
  bucket = each.value.bucket

  block_public_acls   = false
  block_public_policy = false
  ignore_public_acls  = false
  restrict_public_buckets = false
}
# Set a policy on the bucket to allow reads from anywhere
resource "aws_s3_bucket_policy" "site" {
  for_each = aws_s3_bucket.static-content
  bucket = each.value.bucket
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "PublicReadGetObject"
        Effect = "Allow"
        Principal = "*"
        Action = "s3:GetObject"
        Resource = [
          "arn:aws:s3:::${each.value.bucket}",
          "arn:aws:s3:::${each.value.bucket}/*",
        ]
      }
    ]
  })
}



