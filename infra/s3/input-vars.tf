# All variables that are used in various places go here

######################### General provider specific values

variable "aws_key" {
	description = "Access Key for AWS operations"
	type = string
	sensitive = true
}

variable "aws_secret" {
	description = "Secret Key for AWS operations"
	type = string
	sensitive = true
}

variable "aws_region" {
	description = "Region where the VPC is located"
	type = string
	sensitive = true
}

variable "vpc_id" {
  description = "Project Athens VPC ID"
  type = string
}

######################### Alpha Cluster variables

variable "athens_prefix" {
	description = "Prefix for all things in alpha cluster"
	type = string
	default = "athens"
}

######################### Nginx reverse proxy vars


# Yes these buckets _could_ be public but where's the fun in that :x
variable "shockrah_xyz_s3_access_key_id" {
  description = "Acess key for reading public s3 buckets"
  type        = string
  sensitive   = true
}

variable "shockrah_xyz_s3_secret_key" {
  description = "Secret key for reading public s3 buckets"
  type        = string
  sensitive   = true
}

variable "nginx_port" {
  description = "Port for shockrah.xyz"
  type        = number
  default     = 80
}

######################### Nginx reverse proxy vars

variable "shockrah_xyz_bucket" {
  description = "S3 bucket name"
  type        = string
  default     = "shockrah_xyz"
}

variable "resume_shockrah_xyz_bucket" {
  description = "S3 bucket name"
  type        = string
  default     = "resume_shockrah_xyz"
}

variable "temper" {
  type = object({
    cert_arn = string
  })
}

variable "sg" {
  type = object({
    base_ecs        = string
    ecs_web_ingress = string
    lb_health_check = string
  })
}

variable "alpha" {
  type = object({
    dns  = string
    zone = string
  })
}


