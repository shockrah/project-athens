resource "aws_s3_bucket" "static-content" {
  for_each = {
    for idx, record in local.buckets:
      idx => record
  }
  
  bucket = each.value

  tags = {
    Name = each.value
    Description = "Static content"
  }
}




