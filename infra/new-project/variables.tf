# API Keys required to reach AWS/Vultr
variable vultr_api_key {
  type = string
  sensitive = true
}

variable aws_key {
  type = string
  sensitive = true
}

variable aws_secret {
  type = string
  sensitive = true
}

variable aws_region {
  type = string
  sensitive = true
}
