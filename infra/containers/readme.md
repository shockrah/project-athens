What is this
============

Here we contain scripts to build out all the containers that are run.
All of these images are based on images that are made from other projects

docker-compose.yaml
===================

Services that are more/less "special" go here since most of the stuff that is
run on the main host are basically just static html websites

Services & Containers
=====================

| Service    | Docker Image Used        |
|------------|--------------------------|
| Gitea      | gitea/gitea:latest       |
| Act Runner | gitea/act_runner:nightly |

Why the servics above?
======================

The Gitea related services are there so that I can host my own Git projects
away from "Git as a service" services. I have no issue with Github/Gitlab
but I just like being able to host my own stuff when possible :smiley:



