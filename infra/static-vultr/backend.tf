terraform {
  required_version = ">= 0.13"
  backend s3 {
    bucket  = "project-athens"
    key     = "infra/vultr/static-hosts/state/build.tfstate"
    region  = "us-west-1"
    encrypt = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
    vultr = {
      source = "vultr/vultr"
      version = "2.19.0"
    }
  }
}

provider vultr {
  api_key     = var.vultr_api_key
  rate_limit  = 100
  retry_limit = 3
}

provider aws {
  access_key  = var.aws_key
  secret_key  = var.aws_secret
  region      = var.aws_region
  max_retries = 1
}


