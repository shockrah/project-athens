locals {
  buckets = [
    "shockrah.xyz",
    "resume.shockrah",
    "temper.tv"
  ]
}

resource aws_iam_user vultr {
  name = "vultr"
}

data aws_iam_policy_document assume {
  statement {
    actions = [ "sts:AssumeRole" ]
    principals {
      type        = "AWS"
      identifiers = [ aws_iam_user.vultr.arn ]
    }
  }
}

data aws_iam_policy_document vultr {
  statement {
    effect = "Allow"
    actions = [
      "s3:List*",
      "s3:Get*",
      "s3:Describe*"
    ]
    resources = [ "*" ]
  }
}

resource aws_iam_policy vultr {
  name   = "vultr"
  policy = data.aws_iam_policy_document.vultr.json
}

resource aws_iam_role vultr {
  name = "vultr"
  assume_role_policy = data.aws_iam_policy_document.assume.json
}

resource aws_iam_role_policy_attachment vultr {
  role       = aws_iam_role.vultr.name
  policy_arn = aws_iam_policy.vultr.arn
}

resource aws_iam_user_policy_attachment vultr {
  user = aws_iam_user.vultr.name
  policy_arn = aws_iam_policy.vultr.arn
}


# Keys for the user to do stuff
resource aws_iam_access_key vultr {
  user = aws_iam_user.vultr.name
}

