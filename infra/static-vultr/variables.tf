# API Keys required to reach AWS/Vultr
variable vultr_api_key {
  type = string
  sensitive = true
}

variable aws_key {
  type = string
  sensitive = true
}

variable aws_secret {
  type = string
  sensitive = true
}

variable aws_region {
  type = string
  sensitive = true
}

################### ################### ###################

# Host configuration options
variable host {
  type = object({
    plan   = string
    region = string
    os     = string
    name   = string
    backups = object({
      hour = number # hour of the day
      day = number  # 1 = sunday, 7 = saturday
    })
  })
}

variable enable_ssh {
  type    = bool
  default = false
}




