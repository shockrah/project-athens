# Using a single host for this as I'm just looking for a quick and dirty solution
# to host basically everything for now

resource "vultr_instance" "websites" {
  # Core configuration
  plan    = var.host.plan
  region  = var.host.region
  os_id   = var.host.os
  enable_ipv6 = true


  # Enable backups for now since we're getting off of s3 as well at some point
  backups = "enabled"
  backups_schedule {
    type = "weekly"
    dow  = var.host.backups.day
    hour = var.host.backups.hour
  }
  ssh_key_ids = [ vultr_ssh_key.host.id ]
  firewall_group_id = vultr_firewall_group.host.id
  label = "Websites"
}
