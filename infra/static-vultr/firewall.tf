resource vultr_firewall_group host {
  description = "Static host firewall"
}

resource vultr_firewall_rule web_v4 {
  for_each = toset(["80", "443"])
  firewall_group_id = vultr_firewall_group.host.id
  protocol    = "tcp"
  ip_type     = "v4"
  subnet      = "0.0.0.0"
  subnet_size = 0
  port        = each.value
}

resource vultr_firewall_rule web_v6 {
  for_each = toset(["80", "443"])
  firewall_group_id = vultr_firewall_group.host.id
  protocol = "tcp"
  ip_type  = "v6"
  subnet   = "::"
  subnet_size = 0
  port     = each.value
}

# Turn this off when we're not doing maintenance
resource vultr_firewall_rule ssh_v4 {
  count = var.enable_ssh ? 1 : 0
  firewall_group_id = vultr_firewall_group.host.id
  protocol = "tcp"
  ip_type  = "v4"
  subnet   = "0.0.0.0"
  subnet_size = 0
  port     = "22"
}

resource vultr_firewall_rule gitea_ssh {
  firewall_group_id = vultr_firewall_group.host.id
  protocol = "tcp"
  ip_type  = "v4"
  subnet   = "0.0.0.0"
  subnet_size = 0
  port     = "2222"
}


