host = {
  plan   = "vc2-1c-2gb"
  region = "lax"
  os     = 1743
  name   = "project-athens-static-host"
  backups = {
    day  = 2  # Monday
    hour = 7 # midnight
  }
}

enable_ssh = true


