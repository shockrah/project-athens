resource tls_private_key host {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource vultr_ssh_key host {
  name = "static_ssh_key"
  ssh_key = tls_private_key.host.public_key_openssh
}

####################
#    Immich keys   #
####################
resource tls_private_key immich {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource vultr_ssh_key immich {
  name    = "static_ssh_key"
  ssh_key = tls_private_key.immich.public_key_openssh
}
