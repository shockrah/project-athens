output host_ssh_key {
  sensitive = true
  value = tls_private_key.host.private_key_pem
}

output vultr_secret {
  sensitive = true
  value = aws_iam_access_key.vultr.secret
}

output vultr_key_id {
  sensitive = true
  value = aws_iam_access_key.vultr.id
}


output immich_key {
  sensitive = true
  # value     = tls_private_key.host.private_key_openssh
  value     = vultr_instance.immich.default_password
}


