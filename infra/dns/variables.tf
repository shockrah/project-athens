# All variables that are used in various places go here

######################### General provider specific values

variable "aws_key" {
	description = "Access Key for AWS operations"
	type = string
	sensitive = true
}

variable "aws_secret" {
	description = "Secret Key for AWS operations"
	type = string
	sensitive = true
}

variable "aws_region" {
	description = "Region where the VPC is located"
	type = string
	sensitive = true
}


variable "vultr_host" {
  type = string
  description = "IP of the temp Vultr host"
}

