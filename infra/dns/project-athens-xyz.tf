#############################
# project-athens.xyz DNS ZONE
#############################

# This entry is just for the sample service that is just plain nginx
# No TLS will be placed on this just yet as we need to make sure this
# and the load balancer are setup to receive things properly
resource "aws_route53_zone" "project-athens" {
  name    = "project-athens.xyz"
  comment = "Project Athens domain zone"
}


locals {
  project-athens-records = [
    {
      name    = "project-athens.xyz"
      type    = "NS"
      ttl     = 172800
      records = [
        "ns-806.awsdns-36.net.",
        "ns-1881.awsdns-43.co.uk.",
        "ns-1109.awsdns-10.org.",
        "ns-11.awsdns-01.com.",
      ]
    },
    {
      name    = "project-athens.xyz"
      type    = "SOA"
      ttl     = 900
      records = [
        "ns-806.awsdns-36.net. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"
      ]
    }
  ]
}

resource "aws_route53_record" "project-athens-record" {
  for_each = {
    for index, record in local.project-athens-records:
      index => record
  }
  zone_id  = aws_route53_zone.project-athens.id
  name     = each.value.name
  type     = lookup(each.value, "type", "A")
  ttl      = lookup(each.value, "ttl", 300)
  records  = each.value.records
}

