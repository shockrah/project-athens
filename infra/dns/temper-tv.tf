resource "aws_route53_zone" "temper-tv" {
  name    = "temper.tv"
  comment = "Main zone for temper.tv"
}

# Main entry which points to vultr host for now
resource "aws_route53_record" "temper-tv" {
  zone_id = aws_route53_zone.temper-tv.id
  name    = "temper.tv"
  type    = "A"
  ttl     = 300
  records = [ var.vultr_host ]
}

# For email later down the line
resource "aws_route53_record" "temper-tv-txt" {
  zone_id = aws_route53_zone.temper-tv.id
  name    = "temper.tv"
  type    = "TXT"
  ttl     = 300
  records = [
    "v=spf1 include:_mailcust.gandi.net ?all"
  ]
}

resource "aws_route53_record" "temper-tv-mx" {
  zone_id = aws_route53_zone.temper-tv.id
  name    = "temper.tv"
  type    = "MX"
  ttl     = 10800
  records = [
    "10 spool.mail.gandi.net.",
    "50 fb.mail.gandi.net.",
  ]
}
