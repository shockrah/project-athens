#############################
# shockrah.xyz DNS ZONE
#############################

resource "aws_route53_zone" "shockrah-xyz" {
  name    = "shockrah.xyz"
  comment = "Main shockrah.xyz zone - for personal stuff"
}

locals {
  records = [
    {
      name    = "shockrah.xyz"
      type    = "NS"
      ttl     = 172800
      records = [
        "ns-612.awsdns-12.net.",
        "ns-285.awsdns-35.com.",
        "ns-1702.awsdns-20.co.uk.",
        "ns-1360.awsdns-42.org.",
      ]
    },
    {
      name    = "shockrah.xyz"
      type    = "SOA"
      ttl     = 900
      records = [
        "ns-612.awsdns-12.net. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"
      ]
    },
    {
      name    = "shockrah.xyz"
      type    = "TXT"
      ttl     = 300
      records = [ "v=spf1 include:_mailcust.gandi.net ?all" ]
    },
    { name = "www.shockrah.xyz",      records = [ var.vultr_host ] },
    { name = "resume.shockrah.xyz",   records = [ var.vultr_host ] },
    { name = "immich.shockrah.xyz",    records = [ "45.32.92.196" ] },
    { name = "git.shockrah.xyz",    records = [ var.vultr_host ] },
  ]
}

resource "aws_route53_record" "shockrah-xyz-record" {
  for_each = {
    for index, record in local.records:
      index => record
  }

  zone_id = aws_route53_zone.shockrah-xyz.id
  name    = each.value.name
  type    = lookup(each.value, "type", "A")
  ttl     = lookup(each.value, "ttl", 300)
  records = each.value.records
}

# This is our special snowflake """"master record"""" which points the root
# domain to a alias which normally is not allowed however route53 _does_
# allow this to happen. In basically every other case we would need to point
# this root record to an IP and have an LB attach to that LB
resource "aws_route53_record" "shockrah-xyz-vultr" {
  zone_id = aws_route53_zone.shockrah-xyz.id
  name    = "shockrah.xyz"
  type    = "A"
  ttl     = 300
  records = [ "45.32.83.83" ]
}
