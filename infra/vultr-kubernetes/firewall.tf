# Inbound rules for web traffic
resource vultr_firewall_rule web_inbound {
  for_each = toset([for port in [80, 443, 6443] : tostring(port) ])
  firewall_group_id = vultr_kubernetes.athens.firewall_group_id
  #firewall_group_id = vultr_firewall_group.cluster.id
  protocol = "tcp"
  ip_type = "v4"
  subnet = "0.0.0.0"
  subnet_size = 0
  port = each.value
}
