
cluster = {
  region = "lax"
  label   = "athens-cluster"
  version = "v1.30.0+1"
  pool = {
	  plan =  "vc2-1c-2gb"
	  autoscale = true
	  min = 1
	  max = 2
  }
}

lab_domain = "temprah-lab.xyz"

lb_ip4 = "45.32.68.232"


