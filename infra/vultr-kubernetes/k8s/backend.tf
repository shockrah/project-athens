terraform {
  required_version = ">= 0.13"
  backend s3 {
    bucket  = "project-athens"
    key     = "infra/vke/k8s/state/build.tfstate"
    region  = "us-west-1"
    encrypt = true
  }
  required_providers {
    # For interacting with S3
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.30.0"
    }
  }
}

provider aws {
  access_key  = var.aws_key
  secret_key  = var.aws_secret
  region      = var.aws_region
  max_retries = 1
}

provider kubernetes {
  config_path = "terraform.yaml"
}


