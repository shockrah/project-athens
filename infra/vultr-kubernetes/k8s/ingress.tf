resource kubernetes_ingress_v1 athens {
	metadata {
		name = var.shockrahxyz.name
    namespace = kubernetes_namespace.websites.metadata.0.name
		labels = {
			app = "websites"
		}
	}
	spec {
		rule {
      host = "test.shockrah.xyz"
		  http {
		    path {
		      backend {
						service {
						  name = var.shockrahxyz.name
							port {
							  number = 80
							}
						}
		      }
					path = "/"
		    }
		  }
		}
	}
}


resource kubernetes_service athens_lb {
  metadata {
    name = "athens-websites"
    namespace = kubernetes_namespace.websites.metadata.0.name
    labels = {
      app = "websites"
    }
  }
  spec {
    selector = {
      app = kubernetes_ingress_v1.athens.metadata.0.labels.app
    }
    port {
      port = 80
      target_port = 80
    }
    type = "LoadBalancer"
    external_ips = [ var.cluster.ip ]
  }
}

