Plain nginx for now so that we can test out reverse dns
resource kubernetes_pod shockrah {
  metadata {
    name = var.shockrahxyz.name
    namespace = kubernetes_namespace.websites.metadata.0.name
    labels = {
      app = var.shockrahxyz.name
    }
  }
  spec {
    container {
      image = "nginx"
      name  = "${var.shockrahxyz.name}"
      port {
        container_port = 80
      }
    }
  }
}


