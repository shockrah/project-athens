# First we setup the ingress controller with helm


```sh
helm repo add traefik https://helm.traefik.io/traefik
helm repo update
# Now we can install this to our cluster
helm install --kubeconfig config.yaml traefik traefik/traefik
```

# Prove the service is present with

```sh
kubectl --kubeconfig config.yaml get svc
```

# Create the pods

```sh
kubectl --kubeconfig config.yaml -f k8s/nginx-dep.yaml
```

# Expose on port 80

```sh
kubectl --kubeconfig config.yaml -f k8s/nginx-service.yaml
```

# Create ingress on k8s

```sh
kubectl --kubeconfig config.yaml -f k8s/traefik-ingress.yaml
```

# Take the external IP from the ingress 

Put that into terraform's A record for the domain since this is a load balancer
in Vultr ( actual resource apparantly )

# Configure cert-manager for traefik ingress

Using the latest version from here:
https://github.com/cert-manager/cert-manager/releases/download/v1.14.2/cert-manager.crds.yaml

```sh
kubectl --kubeconfig config.yaml \
	apply --validate=false \
	-f https://github.com/cert-manager/cert-manager/releases/download/v1.14.2/cert-manager.yaml
```

# Create the cert issuer and certificate


```sh
kubectl --kubeconfig config.yaml apply -f k8s/letsencrypt-issuer.yaml
kubectl --kubeconfig config.yaml apply -f k8s/letsencrypt-issuer.yaml
```

Because we just have 1 cert for now we are looking for it's status to be `READY`



