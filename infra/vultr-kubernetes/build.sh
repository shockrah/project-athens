#!/bin/bash

set -e

opt=$1
plan=tfplan

build_plan() {
	echo Generating plan
	set -x
	terraform plan -var-file variables.tfvars -input=false -out $plan
}

deploy_plan() {
	terraform apply $plan
}

help_prompt() {
	cat <<- EOF
	Options: plan deploy help
	EOF
}

# Default to building a plan
source ./secrets.sh
case $opt in 
	plan) build_plan;;
	deploy) deploy_plan;;
	*) help_prompt;;
esac

