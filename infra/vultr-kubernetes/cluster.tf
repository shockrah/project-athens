resource vultr_kubernetes athens {
  region = var.cluster.region
  version = var.cluster.version
  label = var.cluster.label
  enable_firewall = true
  node_pools {
    # how many nodes do we want in this pool
    node_quantity = 1
    plan = var.cluster.pool.plan
    label = var.cluster.label
    min_nodes = var.cluster.pool.min
    max_nodes = var.cluster.pool.max
  }
}

output k8s_config {
  value = vultr_kubernetes.athens.kube_config
  sensitive = true
}


