# API Keys required to reach AWS/Vultr
variable vultr_api_key {
  type = string
  sensitive = true
}

variable aws_key {
  type = string
  sensitive = true
}

variable aws_secret {
  type = string
  sensitive = true
}

variable aws_region {
  type = string
  sensitive = true
}

# Cluster configuration
variable cluster {
  type = object({
    region  = string
    label   = string
    version = string
    pool = object({
      plan = string
      autoscale = bool
      min = number
      max = number
    })
  })
}

variable lab_domain {
  type = string
}

variable lb_ip4 {
  type = string
}

