# All variables that are used in various places go here

######################### General provider specific values

variable "aws_key" {
	description = "Access Key for AWS operations"
	type = string
	sensitive = true
}

variable "aws_secret" {
	description = "Secret Key for AWS operations"
	type = string
	sensitive = true
}

variable "aws_region" {
	description = "Region where the VPC is located"
	type = string
	sensitive = true
}

variable "crete_cidr" {
	description = "CIDR block for the servers themselves"
	type = string
}
variable "athens_availability_zone" {
	description = "Availability zone for Project Bucket"
	type = string
}

variable "athens_cidr" {
	description = "VPC Subnet CIDR block"
	type = string
}
