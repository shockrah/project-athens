
resource "aws_vpc" "athens_vpc" {
  cidr_block = var.athens_cidr
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
    Name = "Project Athens VPC"
  }
}

