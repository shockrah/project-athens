# Used to provide internet access for instances in the VPC
resource "aws_internet_gateway" "athens_internet_gateway" {
  vpc_id = aws_vpc.athens_vpc.id

  tags = {
    Name = "Athens Common Internet Gateway in Olypmus"
  }
}
