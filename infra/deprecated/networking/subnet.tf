# This script represents the subnet structure for Crete(primary subnet)

resource "aws_subnet" "crete" {
  vpc_id = aws_vpc.athens_vpc.id
  # 10.0.1.0/24
  cidr_block = var.crete_cidr
  availability_zone = var.athens_availability_zone

  tags = {
    Name = "Crete Subnet"
    Description = "Main subnet for EC2 and Alpha-LB"
  }
}

resource "aws_subnet" "delphi" {
  vpc_id = aws_vpc.athens_vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "us-west-1c"
  tags = {
    Name = "Delphi Subnet"
    Description = "Secondary subnet for the Alpha-LB mostly"
  }
}
