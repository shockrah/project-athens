# NOTE: local traffic route is implied and does not need to be specified
resource "aws_route_table" "crete_route_table" {
  vpc_id = aws_vpc.athens_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.athens_internet_gateway.id
  }

  tags = {
    Name = "Crete IGW Route Table"
  }
}


resource "aws_route_table_association" "crete_gateway_association" {
  subnet_id = aws_subnet.crete.id
  route_table_id = aws_route_table.crete_route_table.id
}

resource "aws_route_table_association" "delphi_gateway_association" {
  subnet_id      = aws_subnet.delphi.id
  route_table_id = aws_route_table.crete_route_table.id
}
