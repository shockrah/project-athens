terraform {
  required_version = ">= 0.13"
  backend "s3" {
      bucket  = "project-athens"
      key     = "infra/networking/state/build.tfstate"
      region  = "us-west-1"
      encrypt = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.13.0"
    }
  }
}

# Base config for using AWS features w/ Terraform
provider "aws" {
  access_key = var.aws_key
  secret_key = var.aws_secret
  region = var.aws_region
  max_retries = 1
}

