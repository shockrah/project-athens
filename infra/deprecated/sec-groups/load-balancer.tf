resource "aws_security_group" "alpha_health_check" {
  name = "Load Balancer Health check"
  vpc_id = data.aws_vpc.athens.id
  egress {
    cidr_blocks = ["10.0.0.0/8"]
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
  }
}

