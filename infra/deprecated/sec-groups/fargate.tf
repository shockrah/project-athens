# Here are general definitions for security rulesets

resource "aws_security_group" "ecs_web_ingress" {
  name = "Alpha-Web-Ingress"
  description = "Allow web traffic into the host"
  vpc_id = data.aws_vpc.athens.id
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
  }
}

resource "aws_security_group" "base_ecs" {
  vpc_id = data.aws_vpc.athens.id
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
  }
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
  }
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 2049
    to_port   = 2049
    protocol  = "tcp"
  }
}



