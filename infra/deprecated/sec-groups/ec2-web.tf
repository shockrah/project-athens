resource "aws_security_group" "general_web_req" {
  name = "Athens General web server ruleset"
  description = "Allowing strictly web traffic"
  vpc_id = data.aws_vpc.athens.id
  # Intake of web requests(only serving TLS enabled traffic)
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 443
    to_port = 443
    protocol = "tcp"
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 80
    to_port = 80
    protocol = "tcp"
  }
  # WARN: Due to the usage of debian based images this rule
  # is effectively required in order to properly update
  # the system as apt mostly talks over port 443(maybe port 80 too?)
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 443
    to_port = 443
    protocol = "tcp"
  }
  # WARN: like 99% certrain apt falls back to port 80 on occasion
  # which means we kinda need egress in to not break when requesting
  # from shitty repos ...
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 80
    to_port = 80
    protocol = "tcp"
  }
}

