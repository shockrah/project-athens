resource "aws_ecs_task_definition" "beta" {
  family = "${var.athens_prefix}-beta"

  network_mode = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn = aws_iam_role.nginx.arn

  cpu = 256
  memory = 512

  container_definitions = jsonencode([
    {
      name = local.nginx_name
      image = "805875567437.dkr.ecr.us-west-1.amazonaws.com/reverse-proxy:latest"
      cpu = 256
      memory = 512
      essential = true
      portMappings = [
        { 
          containerPort = var.nginx_port, 
          hostPort = var.nginx_port
        }
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group  = aws_cloudwatch_log_group.alpha.name
          awslogs-region = "us-west-1"
          awslogs-stream-prefix  = "beta-container"
        }
      }
    }
  ])
  tags = {
    Name        = "${var.athens_prefix}-beta-task-def-container"
    Description = "Reverse proxy for all static content"
  }
}

resource "aws_ecs_service" "beta_reverse_proxy" {
  name              = local.nginx_name
  cluster           = aws_ecs_cluster.alpha.id
  task_definition   = aws_ecs_task_definition.beta.arn
  desired_count     = 1
  launch_type       = "FARGATE"
  # Allowing doubling over when we request a new container for quick refreshes
  # to the ecs service allowing for super fast and 0 down time deployments
  deployment_maximum_percent = 200
  deployment_minimum_healthy_percent = 100
  load_balancer {
    target_group_arn  = var.lb_target_group
    container_name    = local.nginx_name
    container_port    = var.nginx_port
  }

  network_configuration {
    assign_public_ip  = true
    subnets = [
      data.aws_subnet.delphi.id,
      data.aws_subnet.crete.id,
    ]
    security_groups = [
      var.sg.ecs_web_ingress,
      var.sg.base_ecs,
    ]
  }
}
