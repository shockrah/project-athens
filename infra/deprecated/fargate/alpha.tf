# Alpha houses all of our containerized applications
# Essentially it is a cluster with services that we
# choose to expose to the internet in one form or another

# Alpha cluster definition 
###########################
resource "aws_ecs_cluster" "alpha" {
  name = "${var.athens_prefix}-alpha-cluster"
  configuration {
    execute_command_configuration {
      logging = "OVERRIDE"
      log_configuration {
        cloud_watch_log_group_name = aws_cloudwatch_log_group.alpha.name
      }
    }
  }
}

