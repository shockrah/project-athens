data "aws_iam_policy_document" "assume" {
  statement {
    actions = [ "sts:AssumeRole" ]

    principals {
      type        = "Service"
      identifiers = [ "ecs-tasks.amazonaws.com" ]
    }
  }
}

# General ECS Tasks
###################
data "aws_iam_policy_document" "nginx" {
  # Pull images from ECR
  statement {
    effect = "Allow"
    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:BatchGetImage",
      "ecr:GetDownloadUrlForLayer"
    ]
    resources = [ "*" ]
  }
  # General logging to cloudwatch
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:DescribeLogStreams",
      "logs:PutLogEvents",
    ]
    resources = [ "*" ]
  }
  dynamic "statement" {
    for_each = tolist(local.domains)
    content {
      effect    = "Allow"
      actions   = [ "s3:*" ]
      resources  = [ "arn:aws:s3:::${statement.value}" ]
    }
  }
}
resource "aws_iam_policy" "nginx" {
  name = "${var.athens_prefix}-alpha-nginx-policy"
  policy = data.aws_iam_policy_document.nginx.json
}

resource "aws_iam_role" "nginx" {
  name = "${var.athens_prefix}-alpha-nginx-role"
  assume_role_policy = data.aws_iam_policy_document.assume.json
}

resource "aws_iam_role_policy_attachment" "nginx" {
  role        = aws_iam_role.nginx.name
  policy_arn  = aws_iam_policy.nginx.arn
}

