# Fargate configuration


**Now deprecated**! This entire cluster has been removed since I have started moving
everything over to Vultr's Kubernetes Engine instead. This migration is happening
in two phases.

Phase 1: VPS Host migration, where I dump all the sites onto a cheaper single 
host because AWS is just too expensive at this point.


Phase 2: Moving to VKE since I can use K8s and not break the bank just to
host a handful of small personal projects.

## Blue/Green Deployments

Services such as `nginx/beta` ( which serves static content ) are deployed & 
updated using a blue green deployment method which is facilitated by allowing
overflow containers to spawn and replace older containers. This is most common
for `nginx` because we often want to update content stored in S3 however
the container itself will not refresh for sometime so we can force a refresh 
and get content out much faster this way.

## Alpha Cluster

### nginx/beta

Serves static webpages through nginx container. The dockerfile is defined in
`$PROJECT_ROOT/docker/beta/Dockerfile`.

It serves the following webpages:

* resume.shockrah.xyz
* shockrah.xyz
* temper.tv

