resource "aws_ecr_repository" "this" {
  for_each = {
    for index, repo in local.repos:
      index => repo
  }
  name = each.value
}
