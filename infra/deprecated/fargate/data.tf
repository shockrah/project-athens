data "aws_vpc" "athens" {
  id = var.vpc_id
}

data "aws_subnet" "delphi" {
  id = "subnet-0a1943f26e4338cf6"
}

data "aws_subnet" "crete" {
  id = "subnet-09302319a6678643f"
}

