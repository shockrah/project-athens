# Logging Configuration for services inside the cluster
#######################################################

# Alpha Cloudwatch logging configuration
########################################
resource "aws_cloudwatch_log_group" "alpha" {
  name = "${var.athens_prefix}-alpha-log"
  retention_in_days = 7
}

