# All variables that are used in various places go here

######################### General provider specific values

variable "aws_key" {
	description = "Access Key for AWS operations"
	type = string
	sensitive = true
}

variable "aws_secret" {
	description = "Secret Key for AWS operations"
	type = string
	sensitive = true
}

variable "aws_region" {
	description = "Region where the VPC is located"
	type = string
	sensitive = true
}

variable "vpc_id" {
  description = "Project Athens VPC ID"
  type = string
}

variable "athens_prefix" {
	description = "Prefix for all things in alpha cluster"
	type = string
}

######################### Nginx reverse proxy vars

variable "nginx_port" {
  description = "Port for shockrah.xyz"
  type        = number
}

######################### Nginx reverse proxy vars

variable "sg" {
  type = object({
    base_ecs        = string
    ecs_web_ingress = string
    lb_health_check = string
  })
}

variable "lb_target_group" {
  type = string
}

