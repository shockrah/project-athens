locals {
  # ECR 
  repos = [
    "reverse-proxy",
  ]
  domains = [
    "shockrah.xyz",
    "resume.shockrah.xyz",
    "temper.tv"
  ]
  nginx_name = "${var.athens_prefix}-nginx-static-content"
  nginx_hp_check_interval = 300
}

