# Provider variables
####################

variable vultr_api_key {
  type = string
  sensitive = true
}

variable aws_key {
  type = string
  sensitive = true
}

variable aws_secret {
  type = string
  sensitive = true
}

variable aws_region {
  type = string
  default = "us-west-1"
}

variable mail {
  type = object({
    plan   = string
    region = string
    os     = number
    name   = string
  })
}

variable route53_zone_id {
  type = string
  sensitive = true
}
