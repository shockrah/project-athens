    # Mail stuff
  locals {
    records = [
      { name = "mail.shockrah.xyz", records = [ "45.76.78.158" ] },
      { name = "box.mail.shockrah.xyz", records = [ "45.76.78.158" ] },
      { name = "158.78.76.45.in-addr.arpa", type = "PTR", records = [ "box.mail.shockrah.xyz" ] },
      { name = "8.6.8.2.e.9.e.f.f.f.4.0.0.0.4.5.1.d.f.3.1.0.0.6.0.f.9.1.1.0.0.2.ip6.arpa", type = "PTR", records = [ "box.mail.shockrah.xyz" ] },
      # glue records
      { name = "ns1.mail.shockrah.xyz", records = [ "45.76.78.158" ] },
      { name = "ns2.mail.shockrah.xyz", records = [ "45.76.78.158" ] },
      # ipv6 record
      { name = "mail.shockrah.xyz", type = "AAAA", records = [ "2001:19f0:6001:3fd1:5400:04ff:fe9e:2868" ] },
      # For mail in a box safety checks
      { name = "www.mail.shockrah.xyz", records = [ "45.76.78.158" ] },
      { name = "autoconfig.mail.shockrah.xyz", records = [ "45.76.78.158" ] },
      { name = "autodiscover.mail.shockrah.xyz", records = [ "45.76.78.158" ] },

      { name = "www.mail.shockrah.xyz", type = "AAAA", records = [ "2001:19f0:6001:3fd1:5400:04ff:fe9e:2868" ] },
      { name = "autoconfig.mail.shockrah.xyz", type = "AAAA", records = [ "2001:19f0:6001:3fd1:5400:04ff:fe9e:2868" ] },
      { name = "autodiscover.mail.shockrah.xyz", type = "AAAA", records = [ "2001:19f0:6001:3fd1:5400:04ff:fe9e:2868" ] },
      { name = "box.mail.shockrah.xyz", type = "AAAA", records = [ "2001:19f0:6001:3fd1:5400:04ff:fe9e:2868" ] },
      { name = "mta-sts.mail.shockrah.xyz", type = "AAAA", records = [ "2001:19f0:6001:3fd1:5400:04ff:fe9e:2868" ] },
      { name = "mta-sts.box.mail.shockrah.xyz", type = "AAAA", records = [ "2001:19f0:6001:3fd1:5400:04ff:fe9e:2868" ] },
      { name = "mta-sts.mail.shockrah.xyz", records = [ "45.76.78.158" ] },
      { name = "mta-sts.box.mail.shockrah.xyz", records = [ "45.76.78.158" ] },
    ]
}

resource "aws_route53_record" "mail" {
  for_each = {
    for index, record in local.records:
      index => record
  }

  zone_id = aws_route53_zone.shockrah-xyz.id
  name    = each.value.name
  type    = lookup(each.value, "type", "A")
  ttl     = lookup(each.value, "ttl", 300)
  records = each.value.records
}
