# Basic configuration for the mail server itself
# Monthly cost for this should be about 10$ a month
resource vultr_instance mail {
  # Core config
  plan     = var.mail.plan
  region   = var.mail.region
  os_id    = var.mail.os
  enable_ipv6 = true

  # Enable backups of the server in case we lose something for some reason
  backups = "enabled"
  backups_schedule {
    type = "daily_alt_even"
  }

  # Metadata
  hostname = var.mail.name
  label    = var.mail.name
  tags = [
    "Mail server",
    var.mail.name,
  ]
}

resource vultr_reverse_ipv4 mail {
  instance_id = vultr_instance.mail.id
  ip = vultr_instance.mail.main_ip
  reverse = "box.mail.shockrah.xyz"
}

resource vultr_reverse_ipv6 mail {
  instance_id = vultr_instance.mail.id
  ip = vultr_instance.mail.v6_main_ip
  reverse = "box.mail.shockrah.xyz"
}

