locals {
  rules = {
    # https://github.com/mail-in-a-box/mailinabox/blob/main/security.md
    tcp = [22, 25, 53, 80, 443, 465, 587, 993, 995]
    udp = [53]
  }
}
resource vultr_firewall_group mail {
  description = "Mail server  main firewall"
}

# Inbound rules that we need to define for the instance
# Create all the tcp rules of type ipv4
resource vultr_firewall_rule mail_tcp {
  for_each = toset([for v in local.rules.tcp: tostring(v)])
  firewall_group_id = vultr_firewall_group.mail.id
  protocol = "tcp"
  ip_type  = "v4"
  subnet      = "0.0.0.0"
  subnet_size = 0
  port = each.value
}

# Create all the udp rules of type ipv4
resource vultr_firewall_rule mail_udp {
  for_each = toset([for v in local.rules.udp: tostring(v)])
  firewall_group_id = vultr_firewall_group.mail.id
  protocol = "udp"
  ip_type  = "v4"
  subnet      = "0.0.0.0"
  subnet_size = 0
  port = each.value
}
