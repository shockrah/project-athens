resource tls_private_key mail {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# Ensure we can access the keys when we require it
resource local_sensitive_file mail_private_key {
  filename = "${path.module}/mail_pem"
  content  = tls_private_key.mail.private_key_pem
  file_permission = "0600"
}

resource local_sensitive_file mail_public_key {
  filename = "${path.module}/mail_pub"
  content  = tls_private_key.mail.public_key_openssh
  file_permission = "0600"
}

resource vultr_ssh_key mail {
  name = "mail_key"
  ssh_key = tls_private_key.mail.public_key_openssh
}

