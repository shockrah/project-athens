# Base cerificate for shockrah_xyz
##################################
resource "aws_acm_certificate" "temper_tv" {
  domain_name   = "*.temper.tv"
  subject_alternative_names = [ "temper.tv" ]
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

# DNS RECORDS
#############
resource "aws_route53_record" "temper_tv_cert" {
  zone_id   = var.temper_zone
  name      = tolist(aws_acm_certificate.temper_tv.domain_validation_options)[0].resource_record_name
  type      = tolist(aws_acm_certificate.temper_tv.domain_validation_options)[0].resource_record_type
  records   = [ tolist(aws_acm_certificate.temper_tv.domain_validation_options)[0].resource_record_value ]
  ttl       = 300
} 

# Validation configuration blocks used by terraform 
###################################################
resource "aws_acm_certificate_validation" "temper_tv" {
  certificate_arn = aws_acm_certificate.temper_tv.arn
  validation_record_fqdns = [ aws_route53_record.temper_tv_cert.fqdn ]
}


