# Base certificate for project athens
#####################################
resource "aws_acm_certificate" "project_athens_xyz" {
  domain_name       = "*.project-athens.xyz"
  subject_alternative_names = [ "project-athens.xyz" ]
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "project_athens_xyz_cert" {
  zone_id   = var.project_athens_zone
  name      = tolist(aws_acm_certificate.project_athens_xyz.domain_validation_options)[0].resource_record_name
  type      = tolist(aws_acm_certificate.project_athens_xyz.domain_validation_options)[0].resource_record_type
  records   = [ tolist(aws_acm_certificate.project_athens_xyz.domain_validation_options)[0].resource_record_value ]
  ttl       = 300
} 

resource "aws_acm_certificate_validation" "project_athens_xyz" {
  certificate_arn = aws_acm_certificate.project_athens_xyz.arn
  validation_record_fqdns = [ aws_route53_record.project_athens_xyz_cert.fqdn ]
}
