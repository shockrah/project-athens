# Here is the application load balancer that we use for services hosted on ECS
##############################################################################


# The LB that we'll use to move traffic into our services
#########################################################
resource "aws_lb" "alpha" {
    name = "alpha-lb"
    internal = false
    load_balancer_type = "application"
    subnets = [ data.aws_subnet.delphi.id, data.aws_subnet.crete.id ]
    security_groups = [
      var.sg.ecs_web_ingress,
      var.sg.lb_health_check
    ]
    # TODO: change this to true later
    enable_deletion_protection = false
}

## ECS services manage themselves when it comes to registering to the
## target group so we only need to provide the pool
#####################################################################
resource "aws_lb_target_group" "nginx" {
  name = local.nginx_name 
  port = var.nginx_port
  protocol = "HTTP"
  target_type = "ip"
  vpc_id = data.aws_vpc.athens.id
  health_check {
    interval = local.nginx_hp_check_interval
  }
}

resource "aws_lb_listener" "http" {
  load_balancer_arn  = aws_lb.alpha.arn
  port                = 80
  protocol            = "HTTP"
  default_action { 
    type = "redirect"

    redirect {
      port        = 443
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "https" {
  load_balancer_arn = aws_lb.alpha.arn
  port            = 443
  protocol        = "HTTPS"
  ssl_policy      = "ELBSecurityPolicy-2016-08"

  certificate_arn = aws_acm_certificate_validation.shockrah_xyz.certificate_arn
  default_action {
    type = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      message_body = "Literally how"
      status_code  = "400"
    }
  }
}

resource "aws_lb_listener_rule" "beta" {
  listener_arn = aws_lb_listener.https.arn
  priority = 100
  action {
      type = "forward"
      target_group_arn = aws_lb_target_group.nginx.arn
  }
  condition {
    host_header {
      values = local.domains
    }
  }
}


# Certificate attachment for project athens
###########################################
# Additional certificate project-athens
resource "aws_lb_listener_certificate" "alpha_project_athens_cert" {
  listener_arn    = aws_lb_listener.https.arn
  certificate_arn = aws_acm_certificate_validation.project_athens_xyz.certificate_arn
}

resource "aws_lb_listener_certificate" "alpha_temper_tv_cert" {
  listener_arn    = aws_lb_listener.https.arn
  certificate_arn = aws_acm_certificate_validation.temper_tv.certificate_arn
}

