# Base cerificate for shockrah_xyz
##################################
resource "aws_acm_certificate" "shockrah_xyz" {
  domain_name   = "*.shockrah.xyz"
  subject_alternative_names = [ "shockrah.xyz" ]
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

# DNS RECORDS
#############
resource "aws_route53_record" "shockrah_xyz_cert" {
  zone_id   = var.shockrah_zone
  name      = tolist(aws_acm_certificate.shockrah_xyz.domain_validation_options)[0].resource_record_name
  type      = tolist(aws_acm_certificate.shockrah_xyz.domain_validation_options)[0].resource_record_type
  records   = [ tolist(aws_acm_certificate.shockrah_xyz.domain_validation_options)[0].resource_record_value ]
  ttl       = 300
} 

# Validation configuration blocks used by terraform 
###################################################
resource "aws_acm_certificate_validation" "shockrah_xyz" {
  certificate_arn = aws_acm_certificate.shockrah_xyz.arn
  validation_record_fqdns = [ aws_route53_record.shockrah_xyz_cert.fqdn ]
}

