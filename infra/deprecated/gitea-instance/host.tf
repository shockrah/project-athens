# Basic configuration for the gite server itself
# Monthly cost for this should be about 10$ a month
resource vultr_instance gitea {
  # Core config
  plan     = var.gitea.plan
  region   = var.gitea.region
  os_id    = var.gitea.os
  enable_ipv6 = true

  # Enable backups of the server in case we lose something for some reason
  backups = "enabled"
  backups_schedule {
    type = "daily_alt_even"
  }

  # Metadata
  hostname = var.gitea.name
  label    = var.gitea.name
  tags = [
    "Gitea server",
    var.gitea.name,
  ]
}

resource vultr_reverse_ipv4 gitea {
  instance_id = vultr_instance.gitea.id
  ip = vultr_instance.gitea.main_ip
  reverse = "gitea.project-athens.xyz"
}

resource vultr_reverse_ipv6 gitea {
  instance_id = vultr_instance.gitea.id
  ip = vultr_instance.gitea.v6_main_ip
  reverse = "gitea.project-athens.xyz"
}

