resource tls_private_key gitea {
  algorithm = "RSA"
  rsa_bits  = 4096
}

output gitea_ssh_private {
  value = tls_private_key.gitea.private_key_pem
  sensitive = true
}

output gitea_ssh_public {
  value = tls_private_key.gitea.public_key_openssh
  sensitive = true
}

resource vultr_ssh_key gitea {
  name = "gitea_key"
  ssh_key = tls_private_key.gitea.public_key_openssh
}

