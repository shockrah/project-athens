locals {
  # Rules for publicly reaching the gitea instance
  rules = {
    tcp = [22, 25, 53, 80, 443, 465, 587, 993, 995]
  }
}
resource vultr_firewall_group gitea {
  description = "Gitea server  main firewall"
}

# Inbound rules that we need to define for the instance
# Create all the tcp rules of type ipv4
resource vultr_firewall_rule gitea_tcp {
  for_each = toset([for v in local.rules.tcp: tostring(v)])
  firewall_group_id = vultr_firewall_group.gitea.id
  protocol = "tcp"
  ip_type  = "v4"
  subnet      = "0.0.0.0"
  subnet_size = 0
  port = each.value
}

