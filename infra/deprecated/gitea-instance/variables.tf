# Provider variables
####################

# For creating Vultr resources
variable vultr_api_key {
  type = string
  sensitive = true
}

# Using AWS for route53 as this is where we define our DNS entries
variable aws_key {
  type = string
  sensitive = true
}

variable aws_secret {
  type = string
  sensitive = true
}

variable aws_region {
  type = string
  default = "us-west-1"
}

# References the gitea host configuration
variable gitea {
  type = object({
    plan   = string
    region = string
    os     = number
    name   = string
  })
}

# For picking out the zone to create the git.project-athens.xyz DNS entry
variable route53_zone_id {
  type = string
  sensitive = true
}
