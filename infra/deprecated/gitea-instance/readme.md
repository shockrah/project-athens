# Gary Gitea

This folder contains all the infra code for setting up the Gitea
instance. This will be a single public server that contains both
the main Gitea service as well as the CI/CD pipeline that we'll
be using for deployment things. This is all part of a move away
from hosted services such as Gitlab.

# Services To Support

* Gitea Git services
* LFS for ( Temper ) related projects that require larger file storage

## Why the move to self hosted Gitea

Dubious copyright protection/user agreements with SaaS providers.
I would much rather setup my own repositories with all content
in one place that I can manage on my own instead.

## Why Vultr

Two reasons:

* To not rope myself further into using only AWS.

* Good Terraform support which makes the infrastructure setup
relatively painless





