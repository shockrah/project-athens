#!/bin/bash

set -e

bucket="$1"
s3env=/opt/nginx/s3.env

[[ -z "$bucket" ]] && echo "No bucket selected" && exit 1

[[ ! -f $s3env ]] && echo "No credentials to source!" && exit 1
source $s3env

pull() {
	aws s3 sync s3://$bucket /opt/nginx/$bucket
}


case $bucket in 
	resume.shockrah.xyz|shockrah.xyz|temper.tv) pull;;
	*) echo "Invalid bucket name" && exit 1 ;;
esac


