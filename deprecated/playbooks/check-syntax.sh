#!/bin/sh

for f in `find -name '*.yml'`; do
	echo ansible-playbook -i hosts.ini --syntax-check $f
	ansible-playbook -i hosts.ini --syntax-check $f
done
