#!/bin/sh

# -_-

set -x

ssh -F .ssh/config -o UserKnownHostsFile=.ssh/known_hosts $@
