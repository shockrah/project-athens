#!/bin/bash

set -e

# Build the image locally first
docker build . -t reverse-proxy:latest

# Tag as required
docker tag reverse-proxy:latest 805875567437.dkr.ecr.us-west-1.amazonaws.com/reverse-proxy:latest

if [ "$1" = "dev" ]; then
	###########################
	# Development build steps
	###########################
	echo "Building local dev image"
	echo "Skipping docker push because this is a local build"
elif [ "$1" = "prod" ]; then
	###########################
	# Production build steps
	###########################
	echo "Building production image"
	echo "Authenticating to push to production registry"
	# ECR Authentication
	aws ecr get-login-password --region us-west-1 | docker login --username AWS --password-stdin 805875567437.dkr.ecr.us-west-1.amazonaws.com
	# Pushing tagged image
	docker push 805875567437.dkr.ecr.us-west-1.amazonaws.com/reverse-proxy:latest
else
	echo "Unknown option given to build.sh"
	exit 1
fi

