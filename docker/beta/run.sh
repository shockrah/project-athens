#!/bin/bash

set -e

# This script is used for running the image locally for testing purposes

docker run --publish 80:80 --name gateway --rm \
	805875567437.dkr.ecr.us-west-1.amazonaws.com/reverse-proxy:latest
