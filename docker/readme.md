# What is this

This folder contains docker images that live in ECR

## `beta`

Reverse proxy for all things relating to static content under Project Athens.

All static site content lives in S3 and thus this proxies that content.
