# What this covers

The creation of Atlas as it happened in order

## Commands Ran

Once the infra was provisioned and verified to be configured by Terraform correctly
we move on to the following

```sh
# Setup the machine to run docker
ansible-playbook -i hosts.ini atlas/init/system-deps.yml

# Second we copy over the contents of Alpha's mounted docker volumes
ansible-playbook -i hosts.ini atlas/init/perma-mount-drives.yml

# Next we copy over the data that we want to migrate ( if any )
ansible-playbook -i hosts.ini -e filebrowser=/path -e clippable=/path atlas/init/migrate-clips-files.yml

# Setup the services on the host that we want to run
ansible-playbook -i hosts.ini atlas/init/setup-containers.yml

# Next we put up the reverse proxy (nginx)
ansible-playbook -i hosts.ini atlas/init/setup-reverse-proxy.yml

# Finally we add TLS on top of nginx and we're done
ansible-playbook -i hosts.ini atlas/init/setup-certbot.yml
```

Maintenance should be straight forward for this machine as TLS is automatically
renewed every 3 months by a cron job. We can manually update the certs however
if we really want to. They also don't require anymore manual variable injection
like Alpha did as the only thing protected was `dev@shockrah.xyz` which is at
this point becoming semi-public. This means while it is associated with code
it is more of a _business e-mail_ so it can be placed in this repository with
very little concern.

System updates are now also to be fetched with a:

```sh
ansible-playbook -i hosts.ini atlas/maintain/analyze-system-deps.yml
```

Which performs purely read operations and does not affect the state of the
machine.
