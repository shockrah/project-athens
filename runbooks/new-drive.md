# Mounting an attached drive

Assumptions:

* New drive is attached(in AWS) and detected in software
Ideally attachment is made through terraform

## Mounting Instructions (Step-by-Step)

1. Verify data does not have data: `sudo file -s /dev/xvdf`

Should return `data` if its ok. Other wise we're probably looking at the wrong
drive.

2. Create the filesystem on the new empty drive: `sudo mkfs -t ext4 /dev/xvdf`

3. Create mountpoint other wares to actaully use the drive
`sudo mkdir /mnt/example`.

Change _example_ to something that actually makes sense.

4. Add a new entry to /etc/fstab for automounting

`/dev/xvdf       /newvolume   ext4    defaults,nofail        0       0`

Tab delimited btw.

5. Mount all drives listed in `/etc/fstab` from before. `sudo mount -a`





